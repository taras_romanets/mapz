﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class Context
    {
        public Player dino;

        public Player Dino
        {
            get { return dino; }
            set { dino = value; }
        }


        public Context(Player p)
        {
            this.Dino = p;
        }

        public void ChangeState(IStrategy s)
        {
            this.Dino.ChangeState(this, s);
        }
    }
}
