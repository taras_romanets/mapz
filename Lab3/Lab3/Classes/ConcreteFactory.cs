﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class ConcreteFactory : IFactory
    {
        public override Obstacle CreateBird()
        {
            return  new Bird(new PointF(0 + 100 * 9, 210), new Size(50, 50));
        }

        public override Obstacle CreateCactus()
        {
            return new Cactus(new PointF(0 + 100 * 9, 251), new Size(40, 50));
        }
    }
}
