﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class Bird : Obstacle
    {
        int frameCount = 0;
        int animationCount = 0;

        public Bird(PointF point, Size size) : base(point, size)
        {
        }

        public Bird()
        { }


        public override void DrawSprite()
        {
            frameCount++;
            if(frameCount <= 10)
            {
                animationCount = 0;
            }
            else if(frameCount > 10 && frameCount <= 20)
            {
                animationCount = 1;
            }
            else
            {
                frameCount = 0;
            }
        }

        public override void CreatePicture(Graphics g)
        {
            g.DrawImage(GameController.Instance.spriteSheet,
             new Rectangle(new Point((int)transform.position.X, (int)transform.position.Y),
                           new Size(transform.size.Width, transform.size.Height)),
                           264 + 92 * animationCount, 6, 83, 71, GraphicsUnit.Pixel);
        }
    }

}
