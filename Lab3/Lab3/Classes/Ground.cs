﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class Ground
    {
        public Transform transform;

        public Ground(PointF position, Size size)
        {
            transform = new Transform(position, size);
        }

        public void DrawSprite(Graphics g)
        {
            g.DrawImage(GameController.Instance.spriteSheet,
                new Rectangle(new Point((int)transform.position.X, (int)transform.position.Y),
                              new Size(transform.size.Width, transform.size.Height)),
                              2300, 112, 100, 17, GraphicsUnit.Pixel);
        }
    }
}
