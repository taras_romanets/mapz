﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class SlowPhysics : IStrategy
    {
        public SlowPhysics() { }

        public void ApplyPhysics(Player player)
        {
            CalculatePhysics(player);
        }

        public void CalculatePhysics(Player player)
        {
            if (player.physics.transform.position.Y < 250 || player.physics.isJumping)
            {
                player.physics.transform.position.Y += player.physics.gravity;
                player.physics.gravity += player.physics.a;
            }
            if (player.physics.transform.position.Y > 250)
                player.physics.isJumping = false;
        }

        public bool Collide(Player player)
        {
            for (int i = 0; i < GameController.Instance.cactuses.Count; i++)
            {
                var cactus = GameController.Instance.cactuses[i];
                PointF delta = new PointF();
                delta.X = (player.physics.transform.position.X + player.physics.transform.size.Width / 2) - (cactus.transform.position.X + cactus.transform.size.Width / 2);
                delta.Y = (player.physics.transform.position.Y + player.physics.transform.size.Height / 2) - (cactus.transform.position.Y + cactus.transform.size.Height / 2);
                if (Math.Abs(delta.X) <= player.physics.transform.size.Width / 2 + cactus.transform.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= player.physics.transform.size.Height / 2 + cactus.transform.size.Height / 2)
                    {
                        GameController.Instance.cactuses.RemoveAt(i);
                        return true;
                    }
                }
            }
            for (int i = 0; i < GameController.Instance.birds.Count; i++)
            {
                var bird = GameController.Instance.birds[i];
                PointF delta = new PointF();
                delta.X = (player.physics.transform.position.X + player.physics.transform.size.Width / 2) - (bird.transform.position.X + bird.transform.size.Width / 2);
                delta.Y = (player.physics.transform.position.Y + player.physics.transform.size.Height / 2) - (bird.transform.position.Y + bird.transform.size.Height / 2);
                if (Math.Abs(delta.X) <= player.physics.transform.size.Width / 2 + bird.transform.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= player.physics.transform.size.Height / 2 + bird.transform.size.Height / 2)
                    {
                        GameController.Instance.birds.RemoveAt(i);
                        return true;
                    }
                }
            }
            return false;
        }

        public void AddForce(Player player)
        {
            if (!player.physics.isJumping)
            {
                player.physics.isJumping = true;
                player.physics.gravity = -10;
            }
        }
    }
}
