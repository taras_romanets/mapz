﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Rabbit : Animal, IJumpable, IRunable
    {
        protected static int objCount;

        public Rabbit() { objCount++; }
        public Rabbit(string n, string c) : base(n, c)
        {
            Console.WriteLine("Rabbit ctr...");
            objCount++;
        }

        static Rabbit()
        {
            objCount = 0;
            Console.WriteLine("static ctr...");
        }

        public Rabbit(Rabbit other)
        {
            this.name = other.name;
            this.color = other.color;
        }

        public static void initObjCount(int a)
        {
            objCount = a;
        }

        public override void eat()
        {
            base.eat();
            Console.WriteLine($"{this.name} is eating grass...");
        }

        public void jump()
        {
            Console.WriteLine($"{name} is jumping");
        }

        public void run()
        {
            Console.WriteLine($"{name} is runnig");
        }

        public void run(string msg)
        {
            Console.WriteLine($"{msg}");
        }


        public override string ToString()
        {
            return $"name - {this.Name}, color - {this.Color}";
        }

        public override bool Equals(object other)
        {
            return this.Name == (other as Rabbit).Name && this.Color == (other as Rabbit).Color;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode() + this.Color.GetHashCode();
        }

        protected Heart heart;

        public static implicit operator Heart(Rabbit obj)
        {
            return obj.heart;
        }
    }

    class Heart
    {
        public int bpm { get; set; }
    }

}