﻿using System;
using System.Diagnostics;
using System.Linq;


namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Days d = Days.Tuesday | Days.Wednesday;

            //bool b = d.HasFlag(Days.Tuesday);
            int[] a = new int[] { 1, 6 };
            int[] b = new int[] { 1, 6 };


            changeArr(ref a);
            a.ToList().ForEach(x => Console.WriteLine(x));

            changeVal(b);
            b.ToList().ForEach(x => Console.WriteLine(x));





            /*int size = 10000000;
            Stopwatch watch = new Stopwatch();


            
            STest[] s = new STest[size];
                for (int i = 0; i < size; i++)
                {
                    s[i] = new STest(10, 11);
                }

            watch.Start();

            for (int i = 0; i < size; i++)
            {
                s[i].calc();
                //s[i].foo();
            }
            watch.Stop();
            Console.WriteLine($"struct - {watch.ElapsedMilliseconds / 1000.0}s");


            CTest[] c = new CTest[size];
        
                for (int i = 0; i < size; i++)
                {
                    c[i] = new CTest(10, 11);

                }

            watch.Restart();
            for (int i = 0; i < size; i++)
            {
                c[i].calc();
                //c[i].foo();
            }
            watch.Stop();
            Console.WriteLine($"class  - {watch.ElapsedMilliseconds / 1000.0}s");


            SeaTest[] sea = new SeaTest[size];
            
                for (int i = 0; i < size; i++)
                {
                    sea[i] = new SeaTest(10, 11);
                }
           
            watch.Restart();
            for (int i = 0; i < size; i++)
            {
                sea[i].calc();
                //sea[i].foo();
            }
            watch.Stop();

            Console.WriteLine($"sealed - {watch.ElapsedMilliseconds / 1000.0}s");
            watch.Stop();*/


            /*Days day = Days.Monday;

            bool isWeekend = (day == Days.Saturday || day == Days.Sunday);
            Console.WriteLine($"{isWeekend}");

            bool goWork = !(day == Days.Saturday && day == Days.Sunday);
            Console.WriteLine($"{goWork}");*/

            /*Rabbit a = new Rabbit("rabbit1", "white");
            Rabbit b = new Rabbit("rabbit2", "black");
            Rabbit.initObjCount(0);*/

            /*A obj = new A();
            B obj2 = obj;

            obj = (A)obj2;*/



            //int a = 5;
            //object b = a;
            //int c = (int)b;
        }

        [Flags]
        public enum Days
        {
            Monday = 1,
            Tuesday = 2,
            Wednesday = 4,
            Thursday = 8,
            Friday = 16,
            Saturday = 32,
            Sunday = 64,
            TuesdayAndWednesday = 6,
        }


        public static void changeVal(int[] a)
        {
            a = new int[] { 2, 8 };

            a[0] = 5;
        }

        public static void changeArr(ref int[] a)
        {
            a = new int[] { 2, 8 };

            a[0] = 5;
        }

    }

    struct STest
    {
        int a;
        int b;
        public STest(int c, int d)
        {
            a = c; b = d;
        }

        public void calc()
        {
            int temp = a + b * a - b / a + a ^ b;
        }

        public void foo() { }


    }
    class CTest
    {
        int a;
        int b;
        public CTest(int c, int d)
        {
            a = c; b = d;
        }

        public void calc()
        {
            int temp = a + b * a - b / a + a ^ b;
        }

        public void foo() { }

    }

    sealed class SeaTest
    {
        int a;
        int b;

        public SeaTest(int c, int d)
        {
            a = c; b = d;
        }

        public void calc()
        {
            int temp = a + b * a - b / a + a ^ b;
        }

        public void foo() { }

    }
}
