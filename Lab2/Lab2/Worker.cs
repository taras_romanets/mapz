﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Worker
    {
        public Worker() { }
        public Worker(int id, string n) 
        {
            projectID = id;
            name = n;
        }

        public int projectID { get; set; }

        private string name;
        public string Name 
        { get { return name; }
          set { if (value.Length <= 10) { name = value; } } 
        }
    }
}
