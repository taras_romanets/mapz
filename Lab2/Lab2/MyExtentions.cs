﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    static class MyExtentions
    {
        public static void Print(this List<Worker> li)
        {
            li.ForEach(e => Console.WriteLine($"Name: {e.Name}, project ID: {e.projectID}"));
        }

        public static bool IsOddProjectID(this Project p) => p.ID % 2 == 1;

        public static void Write<TSource>(this IEnumerable<TSource> s, Func<TSource, object> f)
        {
            s.ToList().ForEach(e => Console.WriteLine(f(e)));
        }
    }
}
