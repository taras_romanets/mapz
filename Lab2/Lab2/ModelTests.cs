﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class ModelTests
    {
        public List<Worker> Workers 
        {
            get {
                return new List<Worker>()
                {
                    new Worker() { projectID = 1, Name = "Worker1"},
                    new Worker() { projectID = 2, Name = "Worker2"},
                    new Worker() { projectID = 3, Name = "Worker3"},
                    new Worker() { projectID = 4, Name = "Worker3"},
                    new Worker() { projectID = 5, Name = "Worker3"},
                };
            }
        }

        public List<Project> Projects 
        { 
            get {
                return new List<Project>()
                {
                    new Project() { ID = 1, Description = "project 1"},
                    new Project() { ID = 2, Description = "project 2"},
                    new Project() { ID = 3, Description = "project 3"},
                    new Project() { ID = 4, Description = "project 4"},
                    new Project() { ID = 5, Description = "project 5"},
                };
            }
        }
    }
}
